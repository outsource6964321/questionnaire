import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { injectEruda } from "@/utils/eruda";
import { ImagePreview } from "vant";

import "@/assets/css/reset.css";
import "@/assets/css/nprogress.less";
import "vant/lib/index.css";

// 非生产环境添加调试
if (/(localhost)|(192\.168)|(test)/.test(window.location.host)) {
  injectEruda();
}

createApp(App).use(router).use(ImagePreview).mount("#app");
