// 验证手机号
export function isvalidPhone(str) {
  const reg = /^1[3|4|5|6|7|8][0-9]{9}$/;
  return reg.test(str);
}

// 验证mp4
export function isVideo(str) {
  const reg = /^.*\.mp4$/;
  return reg.test(str);
}
