/**
 * @method 注入调试插件
 */
export const injectEruda = () => {
  const script = document.createElement("script");
  script.src = "https://cdn.bootcdn.net/ajax/libs/eruda/2.11.2/eruda.min.js";
  script.onload = () => {
    window?.eruda && window?.eruda.init();
  };
  document.body.appendChild(script);
};
