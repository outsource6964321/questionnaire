import axios from "axios";
import { showLoadingToast, closeToast, showToast, showFailToast } from "vant";

const nAxios = axios.create({
  baseURL: "http://test.wlkfz.site",
  timeout: 10000,
  headers: {
    "Content-Type": "application/json"
  }
});

// 请求拦截器
nAxios.interceptors.request.use(config => {
  const { isLoad = true, method, headers, data } = config;
  if (isLoad) showLoadingToast({ forbidClick: true });

  Object.assign(method === "post" ? config.data : config.params, {
    token: window.localStorage.getItem("__token__")
  });
  return config;
});

// 响应拦截器
nAxios.interceptors.response.use(
  Response => {
    closeToast();
    Response.data = Response.data || {};
    if (Response.data.code === -1) {
      showToast({ message: Response.data.msg || "登录失效" });
      window.localStorage.removeItem("__token__");
      window.history.pushState({}, null, "/login");
      window.location.reload();
      return Promise.reject(Response.data);
    }
    if (Response.data.code === 0) {
      showToast({ message: Response.data.msg || "网络错误" });
      return Promise.reject(Response.data);
    }

    return Response.data;
  },
  error => {
    console.error(error, "error");
    closeToast();
    showFailToast({ message: error.message || "网络错误" });
    return Promise.reject(error);
  }
);

/**
 * @method Get请求
 * @param url 接口地址
 * @param data 参数
 * @param config 配置
 */
const Get = async (url = "", data = {}, config = {}) => {
  return (await nAxios.get(url, { params: data, ...config })) || {};
};

/**
 * @method Post请求
 * @param url  接口地址
 * @param data 参数
 * @param config 配置
 */
const Post = async (url, data = {}, config = {}) => {
  return (await nAxios.post(url, data, config)) || {};
};

export { nAxios, Get, Post };
