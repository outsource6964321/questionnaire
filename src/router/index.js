import { createRouter, createWebHistory } from "vue-router";
import routes from "./routes";
import NProgress from "nprogress";

const router = createRouter({
  history: createWebHistory(),
  routes
});

router.beforeEach((to, from, next) => {
  const title = to.meta.title || "";
  title && (document.title = title);
  if (!NProgress.isStarted()) {
    NProgress.start();
  }
  next();
});

router.afterEach(() => {
  NProgress.done();
});

export default router;
