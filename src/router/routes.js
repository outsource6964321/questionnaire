export default [
  {
    path: "",
    name: "homePage",
    component: () => import("@/views/index/index.vue"),
    meta: { title: "主页" },
    // 路由独享守卫
    beforeEnter: (to, from, next) => {
      const token = window.localStorage.getItem("__token__");
      next(token ? undefined : "/login");
    }
  },
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/login/index.vue"),
    meta: { title: "登录" }
  },
  {
    path: "/signup",
    name: "signup",
    component: () => import("@/views/signup/index.vue"),
    meta: { title: "注册" }
  },
  {
    path: "/question/:id",
    name: "question",
    component: () => import("@/views/question/index.vue"),
    meta: { title: "答题" }
  },
  {
    // 提交
    path: "/check",
    name: "check",
    component: () => import("@/views/check/index.vue"),
    meta: { title: "提交" }
  }
];
