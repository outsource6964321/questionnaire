import { Get, Post } from "@/utils/request";

// 登录
// 参数：phone，password
export const loginApi = async data => {
  const res = await Post("/api/index/login", data);
  window.localStorage.setItem('__token__', res.data.token);
  return res;
};

// 注册
// 参数：phone，password
export const registerApi = data => Post("/api/index/register", data);

// 获取题目列表
export const getQuestionListApi = data => Get("/api/index/getList", data);

// 获取题目详情
// 参数：pid(问卷ID)
export const getQuestionDetailApi = data => Get("/api/index/getQuestion", data);

// 提交答案
// 参数：id(问卷ID)，answer(答案 例：[{"question_id":"题目ID","answer_id":"选择项ID 多选逗号分隔"},{},{}])
export const submitAnswerApi = data => Post("/api/index/submit", data);
